/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c 
 * @brief The library does some basic computations on an array
 *
 * The library executes basic statistics and print functionalities on an array of type unsigned char
 *
 * @author Fahad Rahim Bhatti
 * @date 18 Feb 2019
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned char my_array[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */
  unsigned int array_size = 40;
  /* Statistics and Printing Functions Go Here */
   printf("\n The input array is as follows: \n");
   print_array(my_array, array_size);
   printf("\n\n");
   printf("Following are the basic stats: ");
   print_statistics(my_array, array_size); 
   sort_array(my_array, array_size);
   printf("\n Here is the array again after sorting: \n");
   print_array(my_array, array_size);
   printf("\n");
}

/* Add other Implementation File Code Here */

float find_mean(unsigned char arr[], unsigned int n) {
  int i;
  float mean, sum =0.0;
  for (i = 0; i <n; i++) { 
    sum += arr[i]; //add all
  }
  mean = (sum/n);  //divide by index to take average
  return mean;
}

unsigned char find_maximum(unsigned char arr[], unsigned int n) {
  int i;
  unsigned char max = arr[0];
  for (i = 0; i < n; i++) {
    if (arr[i] > max)  //update the max value if exceeded
      max = arr[i];
  }
  return max;
}

unsigned char find_minimum(unsigned char arr[], unsigned int n) {
  int i;
  unsigned char minimum = arr[0];
  for (i = 0; i < n; i++) {
    if (arr[i] < minimum) //update the minimum value if smaller found
      minimum = arr[i];
  }
  return minimum;
}

float find_median(unsigned char arr[], unsigned int n) {
  int i; 
  float median = 0;
  if (n%2 == 0) // if the number of elements are even
    median = (arr[(n-1)/2] + arr[n/2])/2.0; 
  else          // if the number of elements are odd
    median = arr[n/2];
  return median;
}

void sort_array(unsigned char arr[], unsigned int n)  {
  int x=0, y=0, temp=0;
  for (x=0; x<n; x++) 
  {
    for (y=0; y<n-1; y++) 
    {
      if(arr[y]<arr[y+1])
      {            
        temp	= arr[y];
        arr[y]  = arr[y+1];
        arr[y+1]= temp;
      }
    }
  }
}

void print_array(unsigned char arr[], unsigned int n) {
  for (int c =0; c < n; ++c){
  printf(" %u", arr[c]);
  }
}

void print_statistics(unsigned char arr[], unsigned int n) {
  unsigned char maximum;
  unsigned char minimum;
  float mean;
  float median;
  
    printf("\n"); //newline for viewability

    mean = find_mean(arr, n);
    printf("Mean: %f \n", mean);
  
    median = find_median(arr, n);
    printf("Median: %f \n", median);
    
    maximum = find_maximum(arr, n); 
    printf("Maximum value: %u \n", maximum);

    minimum = find_minimum(arr, n);
    printf("Minimum value: %u \n", minimum);
}
