/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h 
 * @brief A description of stats functions
 *
 * The file contains several useful function declarations to calculate basic statistics and functionalities on an array
 *
 * @author Fahad Rahim Bhatti
 * @date 18 Feb 2019
 *
 */
#ifndef __STATS_H__
#define __STATS_H__


/**
 * @brief Find the maximum value in an array of type unsigned char
 *
 * This function takes an input array of type unsigned char and finds the maximum value
 *
 * @param unsigned char <arr[]> The array on which the operation is to be performed
 * @param unsigned int <n> size of the array
 *
 * @return Maximum value of type unsigned char
 */
unsigned char find_maximum(unsigned char arr[], unsigned int n);


/**
 * @brief Find the minimum value in an array of type unsigned char
 *
 * This function takes an input array of type unsigned char and finds the minimum value
 *
 * @param unsigned char <arr[]> The array on which the operation is to be performed
 * @param unsigned int <n> size of the array
 *
 * @return Minimum value of type unsigned char
 */

unsigned char find_minimum(unsigned char arr[], unsigned int n);

/**
 * @brief Find the median value in an array of type unsigned char
 *
 * This function takes an input array of type unsigned char and finds the median value
 *
 * @param unsigned char <arr[]> The array on which the operation is to be performed
 * @param unsigned int <n> size of the array
 *
 * @return Median value of type float
 */

float find_median(unsigned char arr[], unsigned int n);


/**
 * @brief Find the mean value in an array of type unsigned char
 *
 * This function takes an input array of type unsigned char and finds the mean value
 *
 * @param unsigned char <arr[]> The array on which the operation is to be performed
 * @param unsigned int <n> size of the array
 *
 * @return Mean value of type float
 */
float find_mean(unsigned char arr[], unsigned int n);


/** 
 * @brief Print the array of type unsigned char.
 * 
 * This function takes an input array of type unsigned char and prints it as it is.
 *
 * @param unsigned char <arr[]> The array on which the operation is to be performed
 * @param unsigned int <n> size of the array
 *
 * @return void
 */
void print_array(unsigned char arr[], unsigned int n);

/** 
 * @brief Print the statistics for an array of type unsigned char.
 * 
 * This function takes an input array of type unsigned char and prints its Maximum, Minimum, Median and Mean.
 *
 * @param unsigned char <arr[]> The array on which the operation is to be performed
 * @param unsigned int <n> size of the array
 *
 * @return void
 */
void print_statistics(unsigned char arr[], unsigned int n);

/** 
 * @brief Sorts an array of type unsigned char.
 * 
 * This function takes an input array of type unsigned char and sorts it in descending order.
 *
 *
 * @param unsigned char <arr[]> The array on which the operation is to be performed
 * @param unsigned int <n> size of the array
 * 
 * @return void
 */
void sort_array(unsigned char arr[], unsigned int n);
#endif /* __STATS_H__ */
